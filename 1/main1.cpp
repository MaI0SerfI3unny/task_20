#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

void tokenize(string str, char delim, vector<string> &out)
{
    size_t start;
    size_t end = 0;
    while ((start = str.find_first_not_of(delim, end)) != string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}

bool isNumber(string& s)
{
    for (char &ch : s) 
        if (isdigit(ch) == 0) return false;
    return true;
}

bool validate(vector<string> &out){
    vector<string> date;
    tokenize(out[3],'.',date);
    if(out.size() != 4) return false;
    if(!isNumber(out[2])) return false;
    if(date.size() != 3) return false;
    bool correct = true;
    for(int i = 0; i < date.size(); i++){
        if(!isNumber(date[i])) correct = false;
    }
    if(!correct) return false;
    return true;
}

int main(){
    string usr_string;
    
    while(usr_string != "-"){
        vector<string> out;
        cout << "Введите нового пользователя: ";
        getline(cin,usr_string);
        tokenize(usr_string, ' ', out);
        bool correctValide = validate(out);
        if(correctValide){
            fstream usr_text("list.txt", ios::binary|ios::app);
            usr_text << usr_string << endl;
            usr_text.close();
        }else{
            cerr<<"Проблема с считыванием"<<endl;
        }
    }
    return 0;
}
