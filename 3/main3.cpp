#include <iostream>
#include <fstream>

using namespace std;

int main(){
    int countCatched = 0,
        counter = 0;
    while(true){
        if(counter == 10) counter = 0;
        string catchFish, line;
        cout<<"What fish are we going for today?"<<endl;
        cin>>catchFish;
        if(catchFish == "quit") break;
        
        ifstream river("river.txt", ios::binary);
        int catchNum = 0;
        while(getline(river, line)){
            if(catchNum == counter){
                if(catchFish == line){
                    cout<<"Fish was catched"<<endl;
                    countCatched++;
                    ofstream basket("basket.txt", ios::binary|ios::app);
                    basket << line << endl;
                    basket.close();
                }else{
                    cout<<"Fish wasn`t catched"<<endl;
                }
            }
            catchNum++;
        }
        counter++;
        river.close();
    }
    cout<<"Catch for today: "<< countCatched << endl;
    return 0;
}