#include <iostream>
#include <fstream>

using namespace std;

int main(){
    string chooseOperation;
    int bills[5] = {100,200,500,1000,5000};
    int money[1000];
    cout<<"Выберете операцию"<<endl;
    cin>>chooseOperation;
    ifstream in("money.bin",ios::binary);
    if(in.is_open()){
        in.read((char *) money, sizeof(money));
        in.close();
    }else{
        for(int i=0; i < 1000; ++i)
            money[i] = 0;
    }

    if(chooseOperation == "+"){
        for(int i = 0; i < 1000; ++i){
            if(money[i] == 0){
                money[i] = bills[rand()%5];
            }
        }
        ofstream out("money.bin", ios::binary);
        out.write((char*)money, sizeof(money));
        return 0;
    }else if(chooseOperation == "-"){
        int amount;
        cout<<"Введите количество"<<endl;
        cin>>amount;
        if((amount % 100) != 0){
            cerr<<"Неверное количество "<<amount<<endl;
            return 1;
        }

        int amount_to_collect = amount;
        for(int i = 4; i >= 0; --i){
            int bill = bills[i];
            for(int j = 0; j < 1000; ++j){
                if(money[j] == bill){
                    if(amount_to_collect >= bill){
                        money[j] = 0;
                        amount_to_collect -= bill;
                        if(amount_to_collect == 0){
                            cout<<"Взятая сумма "<<amount<<endl;
                            ofstream out("money.bin", ios::binary);
                            out.write((char*) money, sizeof(money));
                            return 0;
                        } 
                    }
                }
            }
        }
        cerr<<"Не достаточно на счету"<<endl;
        return 1;
    }
}