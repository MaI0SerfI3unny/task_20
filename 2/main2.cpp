#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
using namespace std;

int main(){
    srand(time(nullptr));
    
    int binaryCode[2] = {0,1};
    int width, height;
    
    cin>>width>>height;
    vector <vector<int> > a(width, vector <int> (height) );

    for (int i = 0; i < height; i++)    
        for (int j = 0; j < width; j++) 
        {
           a[i][j] = binaryCode[rand() % 2];
        }
  
    ofstream picter("pic.txt", ios::binary);
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
            picter<< a[i][j];
        picter<<endl;
    }
    picter.close();
    return 0;
}